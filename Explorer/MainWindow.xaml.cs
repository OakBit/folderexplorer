﻿using System;
using System.Collections.ObjectModel;
using System.IO;
using System.Runtime.Remoting.Messaging;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Forms;
using System.Windows.Threading;

namespace Explorer
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    ///
    // Refresh icon Published by Freepik  2 years ago from http://www.flaticon.com/packs/refreshing-and-reloading-lineal
    // Folder icon by Madebyoliver from http://www.flaticon.com/free-icon/folder_148947#term=folder&page=1&position=31
    // File icon by Madebyoliver from http://www.flaticon.com/free-icon/file_149345#term=file&page=1&position=1

    public partial class MainWindow : Window
    {
        //Will only hold a history of 10 at a time, no duplicate entries if one is reselcted it simply gets moved higher on the list
        ObservableCollection<string> ListSearchHistory;

        static ObservableCollection<FolderTree> FolderStructure;
        //Default path is current directory
        DirectoryInfo currentParentFolderName = new DirectoryInfo("./");

        FolderBrowserDialog folderDialog = new FolderBrowserDialog();

        bool LoadingFiles = false;
        //temp log
        static System.Collections.Specialized.StringCollection log = new System.Collections.Specialized.StringCollection();

        public MainWindow()
        {
            InitializeComponent();

            this.Loaded += MainWindow_Loaded;
        }

        /*******************************
        *EVENTS
        *******************************/
        private void MainWindow_Loaded(object sender, RoutedEventArgs e)
        {
            try
            {
                using (StreamReader fs = new StreamReader("./SaveFile.txt"))
                {
                    currentParentFolderName = new DirectoryInfo(fs.ReadLine());
                }
            }
            catch(Exception ex)
            {
                Console.WriteLine(ex.Message);
            }

            FolderStructure = new ObservableCollection<FolderTree>();
            tvFiles.ItemsSource = FolderStructure;

            ListSearchHistory = new ObservableCollection<string>();
            cboSearch.ItemsSource = ListSearchHistory;

            LoadFolderStructure(currentParentFolderName);

            this.DataContext = this;
        }

        private void btnRefresh_Click(object sender, RoutedEventArgs e)
        {
            LoadFolderStructure(currentParentFolderName);
        }

        private void btnSelectFolder_Click(object sender, RoutedEventArgs e)
        {
            
            if (folderDialog.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                currentParentFolderName = new DirectoryInfo(folderDialog.SelectedPath);
                
                LoadFolderStructure(currentParentFolderName);
            }
        }

        private void cboSearch_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if ((string)cboSearch.SelectedValue != currentParentFolderName.FullName)
            {
                currentParentFolderName = new DirectoryInfo((string)cboSearch.SelectedValue);
                LoadFolderStructure(currentParentFolderName);
            }
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            try
            {
                using (StreamWriter fs = new StreamWriter("./SaveFile.txt"))
                {
                    fs.WriteLine(currentParentFolderName.FullName);
                }
            }
            catch(Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        private delegate FolderTree DirectorySearchDelegate(DirectoryInfo root, FolderTree parent);
        public void dirSearchCallback(IAsyncResult theResults)
        {
            AsyncResult result = (AsyncResult)theResults;
            DirectorySearchDelegate del = (DirectorySearchDelegate)result.AsyncDelegate;
            FolderTree answer = del.EndInvoke(theResults);

            this.Dispatcher.Invoke(
                DispatcherPriority.Background, ((Action)(() => {
                    FolderStructure.Add(answer);

                    SetStatus("Finished Loading.");

                    NumFoldersProp = CountFolders(FolderStructure);
                    NumFilesProp = CountFiles(FolderStructure);

                    // Write out all the files that could not be processed.
                    Console.WriteLine("Files with restricted access:");
                    foreach (string s in log)
                    {
                        Console.WriteLine(s);
                    }
                    LoadingFiles = false;
                }
                )));
        }


        /***************************************************
        //UTILITY
        ***************************************************/
        public void AddPathToHistory(string path)
        {
            if (ListSearchHistory.Contains(path))
            {
                cboSearch.SelectedIndex = ListSearchHistory.IndexOf(path);
                return;
            }

            if (ListSearchHistory.Count < 10)
            {
                    ListSearchHistory.Add(path);
            }
            else
            {
                    ListSearchHistory.RemoveAt(0);
                    ListSearchHistory.Add(path);
            }
            cboSearch.SelectedIndex = cboSearch.Items.Count - 1;
        }

        public void LoadFolderStructure(DirectoryInfo path)
        {
            if (LoadingFiles == false)
            {
                LoadingFiles = true;
                SetStatus("Loading files and folders in " + path.FullName);
                //Reset selected counts
                ResetCount();

                FolderStructure.Clear();
                NumFoldersProp = 0;
                NumFilesProp = 0;

                DirectorySearchDelegate searchDirs = new DirectorySearchDelegate(WalkDirectoryTree);

                searchDirs.BeginInvoke(path, null, dirSearchCallback, this);

                AddPathToHistory(path.FullName);

            }
            else
                System.Windows.MessageBox.Show("Please wait for the load to complete before selecting another folder");
        }

        private int CountFolders(ObservableCollection<FolderTree> tree)
        {
            int retVal = 0;
            foreach (FolderTree f in tree)
            {
                if (f.Directory != null)
                    ++retVal;
                if (f.Children.Count > 0)
                   retVal +=CountFolders(f.Children);
            }
            return retVal;
        }

        private int CountFiles(ObservableCollection<FolderTree> tree)
        {
            int retVal = 0;
            foreach (FolderTree f in tree)
            {
                if (f.File != null)
                    ++retVal;
                if (f.Children.Count > 0)
                   retVal +=CountFiles(f.Children);
            }
            return retVal;
        }
        
        private void SetStatus(string s)
        {
            tbStatus.Text = s;
        }

        private void ResetCount()
        {
            SelectedFileSizeProp = 0;
            SelectedFilesProp = 0;
            SelectedFoldersProp = 0;
        }

        static FolderTree WalkDirectoryTree(DirectoryInfo root, FolderTree parent)
        {
            FileInfo[] files = null;
            DirectoryInfo[] subDirs = null;

            FolderTree newTree = new FolderTree(root, parent);

            // First, process all the files directly under this folder
            try
            {
                files = root.GetFiles("*.*");
                foreach (FileInfo fi in files)
                {
                    newTree.Children.Add(new FolderTree(fi, newTree));
                }
            }
            catch (UnauthorizedAccessException e)
            {
                log.Add(e.Message);
            }
            catch (DirectoryNotFoundException e)
            {
                Console.WriteLine(e.Message);
            }

            if (files != null)
            {

                subDirs = root.GetDirectories();

                foreach (System.IO.DirectoryInfo dirInfo in subDirs)
                {
                    // Resursive call for each subdirectory.
                    newTree.Children.Add(WalkDirectoryTree(dirInfo, newTree));
                }
            }

            return newTree;
        }

        /************************************
        *Dependency Properties
        ************************************/


        public int NumFoldersProp
        {
            get { return (int)GetValue(NumFoldersPropProperty); }
            set { SetValue(NumFoldersPropProperty, value); }
        }

        // Using a DependencyProperty as the backing store for NumFoldersProp.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty NumFoldersPropProperty =
            DependencyProperty.Register("NumFoldersProp", typeof(int), typeof(MainWindow), new PropertyMetadata(0));

        //Will use a dependency property for selected files and folders as well, these will be updated if inotifypropertychanged is fired


        public int NumFilesProp
        {
            get { return (int)GetValue(NumFilesPropProperty); }
            set { SetValue(NumFilesPropProperty, value); }
        }

        // Using a DependencyProperty as the backing store for NumFilesProp.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty NumFilesPropProperty =
            DependencyProperty.Register("NumFilesProp", typeof(int), typeof(MainWindow), new PropertyMetadata(0));



        public int SelectedFoldersProp
        {
            get { return (int)GetValue(SelectedFoldersPropProperty); }
            set { SetValue(SelectedFoldersPropProperty, value); }
        }

        // Using a DependencyProperty as the backing store for SelectedFoldersProp.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty SelectedFoldersPropProperty =
            DependencyProperty.Register("SelectedFoldersProp", typeof(int), typeof(MainWindow), new PropertyMetadata(0));



        public int SelectedFilesProp
        {
            get { return (int)GetValue(SelectedFilesPropProperty); }
            set { SetValue(SelectedFilesPropProperty, value); }
        }

        // Using a DependencyProperty as the backing store for SelectedFilesProp.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty SelectedFilesPropProperty =
            DependencyProperty.Register("SelectedFilesProp", typeof(int), typeof(MainWindow), new PropertyMetadata(0));



        public long SelectedFileSizeProp
        {
            get { return (long)GetValue(SelectedFileSizePropProperty); }
            set { SetValue(SelectedFileSizePropProperty, value); }
        }

        // Using a DependencyProperty as the backing store for SelectedFileSizeProp.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty SelectedFileSizePropProperty =
            DependencyProperty.Register("SelectedFileSizeProp", typeof(long), typeof(MainWindow), new PropertyMetadata(0L));

    }
}
