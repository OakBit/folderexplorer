﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace Explorer
{
    public class FolderTree : INotifyPropertyChanged
    {
        //Can be either a file or a folder
        public FolderTree(DirectoryInfo f, FolderTree parent)
        {
            Directory = f;
            Parent = parent;
        }
        public FolderTree(FileInfo f, FolderTree parent)
        {
            File = f;
            Parent = parent;
        }

        public string Name { get { if (Directory != null) return Directory.Name; else return File.Name; } }
        public DirectoryInfo Directory { get; set; }
        public FileInfo File { get; set; }
        public bool IsFolder { get { return Directory != null; } }

        public string ImagePath { get { if (Directory != null) return "Images/folderIcon.png"; else return "Images/fileIcon.png"; } }

        private FolderTree Parent;

        private ObservableCollection<FolderTree> children = new ObservableCollection<FolderTree>();
        public ObservableCollection<FolderTree> Children
        {
            get
            {
                return children;
            }
            set
            {
                children = value;
                NotifyPropertyChanged();
            }
        }

        //Property changed event
        public event PropertyChangedEventHandler PropertyChanged;
        private void NotifyPropertyChanged([System.Runtime.CompilerServices.CallerMemberName] String propertyName = "")
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        private bool? CheckState = false;
        //Can only be : "true", "false", or null
        //Bound to a checkbox and will be set by the checkbox if it is checked
        public bool? IsChecked
        {
            get { return CheckState; }
            set
            {
                if (CheckState != value)
                {
                    UpdateCount(CheckState, value);

                    CheckState = value;
                    if (CheckState == true)
                        foreach (FolderTree t in Children)
                            t.IsChecked = true;
                    else if (CheckState == false)
                        foreach (FolderTree t in Children)
                            t.IsChecked = false;

                    if (Parent != null)
                        Parent.ChildChecked();
                    NotifyPropertyChanged();
                }
            }
        }
        
        public void ChildChecked()
        {
            double count = 0;
            foreach (var c in Children)
            {
                if (c.IsChecked == true)
                    ++count;
                else if (c.IsChecked == null)
                    count += 0.5;
            }
            //Here we set the count indirectly without using the setter 
            //because we dont want to trigger more events from child updates
            if (count == Children.Count)
            {
                UpdateCount(CheckState, true);
                CheckState = true;
                if (Parent != null)
                    Parent.ChildChecked();
                NotifyPropertyChanged("IsChecked");
            }
            else if (count > 0 && count < Children.Count)
            {
                UpdateCount(CheckState, null);
                CheckState = null;
                if (Parent != null)
                    Parent.ChildChecked();
                NotifyPropertyChanged("IsChecked");
            }
            else
            {
                //UpdateCount(CheckState, false);
                CheckState = false;
                if (Parent != null)
                    Parent.ChildChecked();
                NotifyPropertyChanged("IsChecked");
            }

        }

        private void UpdateCount(bool? prev, bool? curr)
        {
            if(prev != curr)
            if (File != null)
            {
                if (curr == true)
                {
                    ++((MainWindow)App.Current.MainWindow).SelectedFilesProp;
                    ((MainWindow)App.Current.MainWindow).SelectedFileSizeProp += File.Length;
                }
                else if (curr == false)
                {
                    --((MainWindow)App.Current.MainWindow).SelectedFilesProp;
                    ((MainWindow)App.Current.MainWindow).SelectedFileSizeProp -= File.Length;
                }
            }
            else if (Directory != null)
            {
                if (curr == true)
                {
                    ++((MainWindow)App.Current.MainWindow).SelectedFoldersProp;
                }
                else if (curr == false)
                {
                    if(prev != null)
                        --((MainWindow)App.Current.MainWindow).SelectedFoldersProp;
                }
                else if (curr == null)
                {
                    if(prev == true)
                        --((MainWindow)App.Current.MainWindow).SelectedFoldersProp;
                }
            }
        }

    }
}
